module.exports = {
  resources: [
    {
      id: "1", // this is compulsory
      name: "Sanyam Mulay",
      designation: "Plugin developer" 
    },
    {
      id: "2",
      name: "Manish Thakur",
      designation: "Plugin User"
    },
    {
      id: "3",
      name: "Anurag Jade",
      designation: "Plugin Dev user"
    },
    {
      id: "4",
      name: "Nikhil",
      designation: "Plugin Dev user"
    }
  ],
  resourceEngagements: [
    {
      resourceId: "1",
      engagementId: "1",
      startTime: "2020-05-19T20:00:00.000+05:30", // ISO string; actually anything that the date constructor accepts
      endTime: "2020-05-20T04:00:00.000+05:30",
      overtimeLimit: 8*60*60, // 8 hours; unit seconds
    },
    {
      resourceId: "2",
      engagementId: "2",
      startTime: "2020-05-19T08:00:00.000+05:30",
      endTime: "2020-05-19T16:00:00.000+05:30",
      overtimeLimit: 8*60*60, // 8 hours; unit seconds
    },
    {
      resourceId: "3",
      engagementId: "3",
      startTime: "2020-05-19T08:00:00.000+05:30",
      endTime: "2020-05-19T16:00:00.000+05:30",
      overtimeLimit: 8*60*60, // 8 hours; unit seconds
    },
    {
      resourceId: "4",
      engagementId: "4",
      startTime: "2020-05-19T09:00:00.000+05:30",
      endTime: "2020-05-19T17:00:00.000+05:30",
      overtimeLimit: 8*60*60, // 8 hours; unit seconds
    }
  ],
  pluginConfig: {
    startTime: "2020-05-19T08:00:00.000+05:30",
    endTime: "2020-05-20T06:00:00.000+05:30",
    stepInterval: 60*60, // 1 hour; unit seconds
    resourceItemClass: "dynamic-class-testing",
    timeColWidth: 100
  }
}