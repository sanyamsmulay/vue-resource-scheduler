import ResourceScheduler from "./components/ResourceScheduler/ResourceScheduler.vue"
import sampleData from "./sampleData.js";

// uncomment if you want the components to be available as soon as imported
// import Vue from 'vue';
// const Components = {
//   ResourceScheduler
// }

// Object.keys(Components).forEach((name) => {
//   Vue.component(name, Components[name]);
// });
// export default Components;

let VueResourceScheduler = ResourceScheduler;

export default VueResourceScheduler;
export { sampleData, VueResourceScheduler };