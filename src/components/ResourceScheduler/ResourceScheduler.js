import { defaultConfig }  from './componentData.js';
import moment  from 'moment';
import VueDraggableResizable from 'vue-draggable-resizable';
// getting default css for the component
import 'vue-draggable-resizable/dist/VueDraggableResizable.css';

// TODO: remove after testing
// console.log("component default config: ", defaultConfig);

const genTimeSlots = function(startTime, endTime, stepInterval) {
  
  // let st = new Date(startTime);
  // TODO: clean up; start time
  // currently assuming it is clean;
  // need to mathematically test below expression 
  // new Date(((st.valueOf()-5.5*3600*1000) - (st.valueOf()-5.5*3600*1000) % (stepInterval*60*1000)) + 5.5*3600*1000)
  let et = new Date(endTime);

  // movingTimePointer
  let mtp = new Date(startTime);
  
  let timeSlots = [];
  
  for (;  mtp < et; mtp.setSeconds(mtp.getSeconds() + stepInterval)) {
    timeSlots.push(new Date(mtp.toISOString()));
  }
  
  return timeSlots;
}

const getStartAndEndTimes = function(left, width, finalTimeColWidth, finalConfig) {
  let newStartTime = new Date( (left / (finalTimeColWidth))*(finalConfig.stepInterval*1000) + 
  new Date(finalConfig.startTime).valueOf());
  // determine new end-time
  let newEndTime = new Date((width / finalTimeColWidth)* finalConfig.stepInterval*1000 + newStartTime.valueOf());
  
  return {
    newStartTime,
    newEndTime
  }
}

const processScheduleSlots = function(resources, resourceEngagements, finalConfig, componentRef) {
  for (let schedule of resourceEngagements) {
    
    // if callbacks already set skip the entry
    if (schedule.dragCallback && schedule.resizeCallback) {
      // console.log("callback already setup, ignoring");
      continue;
    }
    
    let finalTimeColWidth = finalConfig.timeColWidth+(finalConfig.timeColBorder*2);
    let finalResourceRowHeight = finalConfig.resourceRowHeight+(finalConfig.resourceRowBorder*2);
    // determine vertical position
    let resourceIdx = resources.findIndex((resource) => resource.id === schedule.resourceId);
    let top = (resourceIdx + (1 - finalConfig.scheduleRowHeightFraction)/2)*(finalResourceRowHeight) + 
    finalConfig.timeHeaderRowHeight + 2*finalConfig.timeRowBorder;
    schedule.top = top;
    // console.log("top setting to: ", top);
    schedule.topUnbound = top; // can be used by user to reset/cancel move / resize action
    schedule.topInitial = top; // vue-draggable-resizable has issue with y coordinate binding
    
    // determine horizontal position
    let left = ((new Date(schedule.startTime) - new Date(finalConfig.startTime)) / 
      (finalConfig.stepInterval*1000) )*(finalTimeColWidth);
    schedule.left = left > 0 ? left:0;
    schedule.leftUnbound = left; // can be used by user to reset/cancel move / resize action
    schedule.leftInitial = left; // vue-draggable-resizable has issues with x coordinate binding
    
    // determine width of element
    let width = ((new Date(schedule.endTime) - new Date(schedule.startTime)) / (finalConfig.stepInterval*1000) )*
    (finalTimeColWidth);
    schedule.width = width;
    schedule.widthUnbound = width // can be used by user to reset/cancel move / resize action
    
    schedule.dragCallback = (x, y) => {
      // console.log(`dragged schedule #${schedule.resourceId} to (${x}, ${y})`);
      
      // ignore if the movement was tiny (anyway actual movement min. is restricted by grid)
      let maxDist = 2; // in px
      let moved = false;
      if (Math.abs(x - schedule.left) >= maxDist || Math.abs(y - schedule.top) >= maxDist) {
        moved = true;
        // console.log("dist x:", Math.abs(x - schedule.left));
        // console.log("dist y:", Math.abs(y - schedule.top));
      }
      if (!moved) {
        return false;
      }
      
      // determine new resourceId
      let newResourceId = Math.round((y  - (finalConfig.timeHeaderRowHeight + 2*finalConfig.timeRowBorder) -
        (finalResourceRowHeight*(1 - finalConfig.scheduleRowHeightFraction)/2))
        /finalResourceRowHeight) + 1;
      newResourceId = String(newResourceId);
      // determine new start-time and end-time
      let stEtObj = getStartAndEndTimes(x, schedule.width, finalTimeColWidth, finalConfig);
      
      // sync bound variables
      schedule.top = y;
      // console.log("set top to: ", y);
      schedule.left = x;
      
      componentRef.$emit('scheduleMoved', schedule, newResourceId, stEtObj.newStartTime, stEtObj.newEndTime);
    };
    
    // eslint-disable-next-line no-unused-vars
    schedule.resizeCallback = (left, top, width, height) => {
      // console.log(`resized schedule #${schedule.resourceId}, to left:${left}, top:${top}, width: ${width}, height:${height})`)
      
      // determine new start-time and end-time
      // startTimeEndTimeObject 
      let stEtObj = getStartAndEndTimes(left, width, finalTimeColWidth, finalConfig);
      
      // sync bound variables
      schedule.top = top
      schedule.left = left
      schedule.width = width
      
      // console.log("newStartTime", newStartTime, "newEndTime", newEndTime);
      componentRef.$emit('scheduleResized', schedule, stEtObj.newStartTime, stEtObj.newEndTime);
    }
    
    // console.log("new processed schedule obj: ", schedule);
  }
}


export default {
  name: 'ResourceScheduler',
  components: {
    VueDraggableResizable
  },
  props: {
    data: Object,
    config: Object
  },
  data() {
    let finalConfig = defaultConfig;
    if (this.config) {
      Object.assign(finalConfig, this.config);
    }
    // generate time slots
    let timeSlots = genTimeSlots(finalConfig.startTime, finalConfig.endTime, finalConfig.stepInterval);
    
    return {
      // merge default and user supplied config; with user supplied config taking precedence
      finalConfig: finalConfig,
      timeSlots: timeSlots,
      clickCounter: 0,
      clickTimer: null,
      doubleClickDelay: 300, // millisecond
      directiveMouseDnAt: null
    }
  },
  filters: {
    dateToHour: function(date) {
      if (!date) return '';
      // return moment(date).format('H A');
      let dtStr = moment(date).format('h A');
      return dtStr;
    }
  },
  directives: {
    "click-handler": {
      // eslint-disable-next-line no-unused-vars
      bind: (el, binding, vNode) => {
        el.__clickItem__ = binding.value.item;
        el.__clickFn__ = binding.value.handler;
        // eslint-disable-next-line no-unused-vars
        el.__mouseUpHandler__ = (e) => {
          let maxDist = 2; // pixels // TODO: get this from config
          // moved; do not emit if moved a lot
          let moved = false;
          let newX = e.clientX;
          let newY = e.clientY
          let oldX = el.__mouseDnX__ ? el.__mouseDnX__:e.clientX;
          let oldY = el.__mouseDnY__ ? el.__mouseDnY__:e.clientY;
          // TODO: abstract this logic into a function; is repeated in drag
          if (Math.abs(newX - oldX) >= maxDist || Math.abs(newY - oldY) >= maxDist) {
            moved = true;
          }
          if (!moved && el.__directiveMouseDnAt__) {
            // emit long-click if taken too long
            if (new Date() - el.__directiveMouseDnAt__ >= 400) {
              // console.log("Long click happened")
            } else {
              // emit click if within limits
              el.__clickFn__(el.__clickItem__);
            }
            // reset timer
            delete el.__directiveMouseDnAt__;
            delete el.__mouseDnX__;
            delete el.__mouseDnY__;
          }
          // remove listener
          el.removeEventListener('mouseup', el.__mouseUpHandler__, false);
        }
        // eslint-disable-next-line no-unused-vars
        el.__mouseDnHandler__ = (e) => {
          el.__directiveMouseDnAt__ = new Date();
          el.__mouseDnX__ = e.clientX;
          el.__mouseDnY__ = e.clientY;
          el.addEventListener('mouseup', el.__mouseUpHandler__, false);
        }
        
        el.addEventListener('mousedown', el.__mouseDnHandler__, false);
      },
      unbind: (el) => {
        el.removeEventListener('mousedown', el.__mouseDnHandler__, false);
        delete el.__mouseDnHandler__;
        delete el.__mouseUpHandler__;
        delete el.__clickFn__;
        delete el.__clickItem__;
      }
    }
  },
  methods: {
    handleScheduleItemClicks(scheduleItem) {
      this.clickCounter++;
      if (this.clickCounter === 1) {
        
        this.clickTimer = setTimeout(() => {
          // timer expired
          // emit single click event
          this.clickCounter = 0;
          this.$emit("schedule-item-click", scheduleItem)  
        }, this.doubleClickDelay)
      } else {
        // timer not expired
        // clear timer 
        clearTimeout(this.clickTimer)
        // emit double click event
        this.clickCounter = 0;
        this.$emit("schedule-item-dbl-click", scheduleItem);         
      }
    },
    // eslint-disable-next-line no-unused-vars
    handleDragStart(x,y) {
      if (y < (this.finalConfig.timeHeaderRowHeight + 2*this.finalConfig.timeRowBorder)) {
        // console.log("stoppped dragging into top");
        return false; // programmatically stopped dragging into the time header  
      }
    },
    cellClickHandler(resourceObj, startTime) {
      this.$emit("timeCellClick", resourceObj, startTime)
    }
  },
  computed: {
    processedResourceEngagements: function() {
      // console.log("change detection triggered");
      processScheduleSlots(this.data.resources, this.data.resourceEngagements, this.finalConfig, this);
      return this.data.resourceEngagements;
    }
  }
}