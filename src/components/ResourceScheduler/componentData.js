module.exports = {
  defaultConfig: {
    timeHeaderRowHeight: 50, // pixels
    timeColWidth: 50, // pixes
    timeColBorder: 1, // pixels
    timeRowBorder: 1, // pixels
    // resource component
    resourceRowWidthPercent: 20, // percentage
    resourceRowHeight: 100, // pixels
    resourceRowBorder: 1, // pixels
    resourceCustomComponent: null,
    
    // schedule height fraction of row
    scheduleRowHeightFraction: 0.8,
    scheduleCustomComponent: null,
    scheduleKeyBinding: "engagementId"
  }
};