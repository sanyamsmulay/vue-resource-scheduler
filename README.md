# vue-resource-scheduler

A scheduler for displaying resource utilization. Time on the horizontal axis.

## Dependancies

[VueDraggableResizable 2](https://github.com/mauricius/vue-draggable-resizable)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
